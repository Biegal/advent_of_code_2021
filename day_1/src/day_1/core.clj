(ns day-1.core)
(require '[clojure.string :as str])

(defn part1 [& args]
  (println "Hello Day 1!")
  (println
   (reduce (fn [sum pair]
             (if (< (first pair) (last pair)) (inc sum) sum))
           0 (partition 2 1 (map #(Integer/parseInt %) (str/split (slurp "input.txt") #"\n"))))))

(defn part2 [& args]
  (println "Hello Day 1 Part 2!")
  (println
   (reduce (fn [sum pair]
             (if (< (first pair) (last pair)) (inc sum) sum))
           0 (partition 2 1 (reduce (fn [set triple]
                                      (conj set (reduce + triple))) [] (partition 3 1 (map #(Integer/parseInt %) (str/split (slurp "input.txt") #"\n"))))))))

(defn -main [& args]
  (part1 args)
  (part2 args))