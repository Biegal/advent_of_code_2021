(ns day-2.core)
(require '[clojure.string :as str])

(def commands (str/split (slurp "input.txt") #"\n"))

(defn calculatePosition [commands]
  (reduce (fn [position command]
            (let [splitted (str/split command #" ")]
              (println position)
              (cond
                (= "up" (first splitted)) (list (first position) (second position) (- (nth position 2) (Integer/parseInt (second splitted))))
                (= "down" (first splitted)) (list (first position) (second position) (+ (nth position 2) (Integer/parseInt (second splitted))))
                (= "forward" (first splitted)) (list (+ (first position) (* (nth position 2) (Integer/parseInt (second splitted)))) (+ (second position) (Integer/parseInt (second splitted))) (nth position 2))
                )))
          `(0 0 0) commands)) ;; depth horizontal aim

(defn part1 [& args]
  (println "Hello Day 2!")
  (let [position (calculatePosition commands)]
    (println position)
    (println (* (first position) (second position))))
  )

(defn -main [& args]
  (part1 args))